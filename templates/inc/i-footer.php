
			<footer>
				
				<a href="#" class="footer-logo">
					<img src="../assets/bin/images/the-energy-company-light.svg" alt="The Energy Company">
				</a>
				
				<div class="footer-nav">
					<ul>
						<li><a href="#">Strength</a></li>
						<li><a href="#">Corporate</a></li>
						<li><a href="#">Safety</a></li>
						<li><a href="#">The Latest</a></li>
						<li><a href="#">Contact</a></li>
						<li><a href="#">FAQ</a></li>
					</ul>
				</div><!-- .footer-nav -->
				
				<div class="footer-blocks">
				
					<div class="footer-block">
						
						<span class="circle-button blue fa-map-marker">Location</span>
						
						<address>
							25 White Rose Drive <br />
							St. John's, NL A1A 0L2
						</address>
						
					</div><!-- .footer-block -->
					
					<div class="footer-block">
						
						<span class="circle-button blue fa-phone">Phone</span>
						
						<span class="block">709 123 4567</span>
						<span class="block">709 123 8910</span>
						
					</div><!-- .footer-block -->
					
					<div class="footer-block">
						
						<span class="circle-button blue fa-link">Links</span>
						
						<?php include('inc/i-social.php'); ?>
						
					</div><!-- .footer-block -->
					
				</div><!-- .footer-blocks -->
				
				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">The Energy Company</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/bin/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
			
		<script>
			var templateJS = {
				templateURL: 'http://127.0.0.1/energy',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script data-main="../assets/bin/js/main.js" src="../assets/bin/lib/js/requirejs/require.min.js"></script>
	</body>
</html>