<div class="hero">
	<div class="hero-content">
		<img src="../assets/bin/images/the-energy-company-light.svg" alt="The Energy Company">
		
		<span class="hero-title">Your Energy Starts Here</span>
		<span class="hero-subtitle">The Energy Company is a fitness &amp; health company based in St. John's, NL</span>
		<a href="#" class="button big">Find Out More</a>
		
	</div><!-- .hero-content -->
</div><!-- .hero -->