<div class="social">
	<a href="#" title="Like Energy on Facebook" class="circle-button sm blue inverse fb" rel="external">Like Energy on Facebook</a>
	<a href="#" title="Follow Energy on Twitter" class="circle-button sm blue inverse tw" rel="external">Follow Energy on Twitter</a>
	<a href="#" title="Connect with Energy on LinkedIn" class="circle-button sm blue inverse in" rel="external">Connect with Energy on LinkedIn</a>
	<a href="#" title="Watch Energy's Youtube Channel" class="circle-button sm blue inverse yt" rel="external">Watch Energy's YouTube Channel</a>
</div><!-- .social -->