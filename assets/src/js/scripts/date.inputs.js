define([
	'modules/datepicker/datepicker.dev'
],function() {

	var els = $('span.input.date,span.date-input');

	//date inputs
	els.each(function() {
	
		var 
			scriptDfd,
			cssDfd = new $.Deferred(),
			buildDatePickers = function() {
			
				var 
					now  	= (new Date()).setHours(0,0,0,0),
					current	= (new Date()),
					num_calenders = 1;
					current.setMonth(new Date(now).getMonth());
					
				DPOptions = {
					eventName		:	'focus',
					date					:	current,
					current				:	current,
					calendars			:	num_calenders,
					starts				:	0,
					format				:	'b d, Y',
					view					:	'days',
					extraHeight		:	true,
					onBeforeShow	:	function() {
						var 
							el = $(this),
							d = el.data();
					},
					onRender : function(date) {
						var 
							linkedEl,
							linkedElDate,
							el = $(this.el),
							thisDate = el.data('hasdate') ? el.DatePickerGetDate() : null,
							disabled = ( date.valueOf() < now.valueOf() ),
							theClass = '';
						
						theClass += date.valueOf() === now.valueOf() ? ' today' : '';
						theClass += (thisDate && date.valueOf() == thisDate.valueOf()) ? ' actualDatepickerSelected' : '';
						
						if(el.data('linked')) {
							linkedEl = $(el.data('linked'));
							
							if(linkedEl.data('hasdate')) {
								linkedElDate = linkedEl.DatePickerGetDate();
								if(linkedElDate instanceof Date) {
								
									if(date.valueOf() === linkedElDate.valueOf()) {
										theClass += ' linkedDate';
									}
								
									if(linkedEl.data('start')) {
										if(date.valueOf() <= linkedElDate.valueOf()) {
											disabled = true;
										}
										
										if(thisDate) {
											if(date.valueOf() < thisDate.valueOf() && date.valueOf() >= linkedElDate.valueOf()) {
												theClass += ' linkedRange';
											}
										}
										
										
									} else if(linkedEl.data('finish')) {
									
										if(thisDate) {
											if(date.valueOf() > thisDate.valueOf() && date.valueOf() <= linkedElDate.valueOf()) {
												theClass += ' linkedRange';
											}
										}
									
										if(date.valueOf() >= linkedElDate.valueOf()) {
											//disabled = true;
										}
									
									}
								
								}
							}
							
						}
						
						return { 
							disabled : disabled, 
							className : theClass
						};
					},
					onChange : function(format,dateObj,input) {
						var $input = $(input);
						$input.data('hasdate',true);
						$input.DatePickerSetDate(dateObj);
						$input.val(format).DatePickerHide();
						
						if($input.data('linked')) {
							var linkedEl = $($input.data('linked'));
							
							if($input.data('start')) {
								//get the datepicker of the linkedEl and set current
								var 
									shouldUpdate = false,
									newEndDate = new Date( (dateObj.valueOf() + (60*60*24*1000) ) );
									
									if(linkedEl.data('hasdate')) {
										if(dateObj.valueOf() >= linkedEl.DatePickerGetDate().valueOf() ) {
											shouldUpdate = true;
										}
									} else {
										shouldUpdate = true;
									}
									
									if(shouldUpdate) {
										linkedEl
											.val('')
											.removeData('hasdate')
											.DatePickerSetDate(newEndDate,true)
											.focus();									
									}
							}

						}
						
					}
				};
			
				els.each(function() {
				
					if(Modernizr.touch) {
						
						var
							originalInput = $(this).find('input'),
							newInput = originalInput.clone();
						
						newInput.attr('type','date');
						newInput
							.on('focus',function() {
								$(this).parent().addClass('hasdate');
							})
							.on('change blur',function() {
								if(!$.trim($(this).val()).length) {
									$(this).parent().removeClass('hasdate');
								}
							});
						originalInput.replaceWith(newInput);
						newInput.before('<span class="input-label">'+originalInput.attr('placeholder')+'</span>');
						return;
					}
					
					$(this)
						.children()
						.on('keydown',function(e) { if(e.keyCode == 9) { $(this).DatePickerHide(); } })
						.DatePicker(DPOptions)
						.on('focus',function(e) {
							var 
								el = $(this),
								data = el.data();

							if(data['staticdp']) {
								$('#'+data.datepickerId).addClass('appended').insertAfter(el.parent());
							}

						});

				});
			
			};
	
		if($.fn.DatePicker || Modernizr.touch) {
			buildDatePickers.apply();
			return false;
		}
		
		
		return false;
	});
	
	//no public api
	return {};
		
});