define([
	'scripts/debounce',
],function(debounce) {

	var 
		scrollDebounce = debounce(),
		resizeDebounce = debounce(),
		$window = $(window),
		$document = $(document),
		$html = $('html'),
		$nav = $('nav'),
		$navWrap  =$('div.nav'),
		$body = $('body'),
		$pageWrapper = $('div.page-wrapper'),
		startingNavHeight = $navWrap.height(),
		SHOW_CLASS = 'show-nav',
		SMALL_NAV = 'small-nav';

	var methods = {
	
		checkShowSmallNav: function() {
			
			if($window.scrollTop() > startingNavHeight) {
				$body.addClass(SMALL_NAV);
			} else {
				$body.removeClass(SMALL_NAV);
			}
		},

		onScroll: function() {
			this.checkShowSmallNav();
		},
		
		onResize: function() {
			this.checkShowSmallNav();
		},
	
		showNav: function(show) {
			$html[show ? 'addClass' : 'removeClass'](SHOW_CLASS);
		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_CLASS);
		}

	};
	
	//listeners
	$document
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('keydown',function(e) {
			if(e.result !== false && e.keyCode === 27 && methods.isShowingNav()) {
				methods.showNav(false);
				return false;
			}
		});

	$window
		.on('scroll',function() {
			scrollDebounce.requestProcess(methods.onScroll,methods);
		})
		.on('resize',function() {
			resizeDebounce.requestProcess(methods.onResize,methods);
		});
		
		if(Modernizr.ios) {
			
			//onScroll must be fired continuously
			(function iOSOnScroll() {
				methods.onScroll();
				requestAnimationFrame(function() { iOSOnScroll(); });
			}());
		
		} else {
			//fire immediately
			methods.onScroll();
			methods.onResize();
		}
		

	//no public API
	return {};

});